let currentPlayer = 'X';
let arr = Array(9).fill(null);
let moves = [];
let main = document.getElementById('main');
let gridItems = document.querySelectorAll('.box');
let winnerName = document.getElementById('winner');
let result = document.querySelector('.result');
let resultTitle = document.getElementById('result-title');
let currentURL = window.location.href;

function checkWinner() {
    if ((arr[0] !== null && arr[0] == arr[1] && arr[1] == arr[2]) ||
        (arr[3] !== null && arr[3] == arr[4] && arr[4] == arr[5]) ||
        (arr[6] !== null && arr[6] == arr[7] && arr[7] == arr[8]) ||
        (arr[0] !== null && arr[0] == arr[3] && arr[3] == arr[6]) ||
        (arr[1] !== null && arr[1] == arr[4] && arr[4] == arr[7]) ||
        (arr[2] !== null && arr[2] == arr[5] && arr[5] == arr[8]) ||
        (arr[0] !== null && arr[0] == arr[4] && arr[4] == arr[8]) ||
        (arr[2] !== null && arr[2] == arr[4] && arr[4] == arr[6])
    ) {
        winningPopup();
    } else {
        if (!arr.some(e => e === null)) {
            drawPopup();
        }
    }
};


let handleClick = (elm) => {
    const id = Number(elm.id);
    if (arr[id] != null) return;
    arr[id] = currentPlayer;
    moves.push(id);
    elm.innerHTML = currentPlayer;
    checkWinner();
    if (currentPlayer === 'X') {
        currentPlayer = 'O';
    } else if (currentPlayer === 'O') {
        currentPlayer = 'X';
    };
};


function resetGame() {
    for (let i = 0; i < arr.length && i < gridItems.length; i++) {
        arr[i] = null;
        gridItems[i].innerHTML = '';
        currentPlayer = 'X';
    }
}


let undoMove = () => {
    if (moves.length === 0) return;
    const lastMove = moves.pop();
    arr[lastMove] = null;
    document.getElementById(lastMove).innerHTML = '';
    currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
};


function winningPopup() {
    winnerName.innerText = currentPlayer;
    result.showModal();
};

function drawPopup() {
    resultTitle.innerHTML = 'Draw!';
    result.showModal();
};

function playagain() {
    resetGame();
    result.close()
}


const shareData = {
    title: "Tic Tac Toe",
    text: "Play Tic Tac Toe",
    url: currentURL,
};

const shareBtn = document.querySelector("#share");

shareBtn.addEventListener("click", async () => {
    try {
        await navigator.share(shareData);
        console.log('Game shared successfully');
    } catch (err) {
        console.log(`Error: ${err}`);
    }
});
